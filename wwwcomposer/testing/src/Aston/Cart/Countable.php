<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 15/01/19
 * Time: 16:17
 */

namespace Aston\Cart;

/**
 * Interface Countable
 * @package Aston\Cart
 */
interface Countable
{
    /**
     * @param $item
     * @return int
     */
    public function countUp($item) : int;


    /**
     * @param $item
     * @return int
     */
    public function countDown($item) :int;
}
